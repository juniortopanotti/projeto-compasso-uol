package com.topanotti.compassouolprojeto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompassoUolProjetoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompassoUolProjetoApplication.class, args);
	}

}
