package com.topanotti.compassouolprojeto.model;

import lombok.AllArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@AllArgsConstructor
@Entity
@Table(name = "products", schema = "public")
@SequenceGenerator(name = "products_seq", sequenceName = "products_seq", allocationSize = 1)
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="QUINA_SEQ")
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    @Size(max = 50, message = "O campo nome não pode exceder 50 caracteres.")
    @NotNull(message = "O campo name não pode ser nulo.")
    private String name;

    @Column(name = "description")
    @Size(max = 200, message = "O campo description não pode exceder 50 caracteres.")
    @NotNull(message = "O campo description não pode ser nulo.")
    private String description;

    @Column(name = "price")
    @NotNull(message = "O campo price não pode ser nulo.")
    private BigDecimal price;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
