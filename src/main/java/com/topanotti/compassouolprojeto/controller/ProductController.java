package com.topanotti.compassouolprojeto.controller;

import com.topanotti.compassouolprojeto.model.Product;
import com.topanotti.compassouolprojeto.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<Product> insert(@Valid @RequestBody Product product) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.productService.save(product));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable("id") Long id, @Valid @RequestBody Product product) {
        product.setId(id);
        return ResponseEntity.ok(this.productService.save(product));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> get(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.productService.get(id));
    }

    @GetMapping
    public ResponseEntity<List<Product>> getAll() {
        return ResponseEntity.ok(this.productService.getAll());
    }

    @GetMapping("/search")
    public ResponseEntity<List<Product>> getAllSearch(@RequestParam Optional<String> q,
                                                      @RequestParam("min_price") Optional<BigDecimal> minPrice,
                                                      @RequestParam("max_price") Optional<BigDecimal> maxPrice) {
        return ResponseEntity.ok(this.productService.getSearch(minPrice, maxPrice, q));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        this.productService.delete(id);
        return ResponseEntity.ok().build();
    }

}
