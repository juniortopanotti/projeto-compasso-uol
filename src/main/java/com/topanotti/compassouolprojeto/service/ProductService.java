package com.topanotti.compassouolprojeto.service;

import com.querydsl.core.BooleanBuilder;
import com.topanotti.compassouolprojeto.model.Product;
import com.topanotti.compassouolprojeto.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.topanotti.compassouolprojeto.model.QProduct.product;


@Component
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product save(Product entity) {
        if (entity.getId() != null) {
            get(entity.getId());
        }
        return productRepository.save(entity);
    }

    public Product get(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public List<Product> getSearch(Optional<BigDecimal> minPrice, Optional<BigDecimal> maxPrice, Optional<String> q) {
        BooleanBuilder where = new BooleanBuilder();
        if (minPrice.isPresent()) {
            where.and(product.price.goe(minPrice.get()));
        }
        if (maxPrice.isPresent()) {
            where.and(product.price.loe(maxPrice.get()));
        }
        if (q.isPresent()) {
            where.and(product.name.toUpperCase().contains(q.get().toUpperCase()).or(
                    product.description.toUpperCase().contains(q.get().toUpperCase())
            ));
        }

        return StreamSupport.stream(productRepository.findAll(where).spliterator(), false)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        get(id);
        productRepository.deleteById(id);
    }
}
