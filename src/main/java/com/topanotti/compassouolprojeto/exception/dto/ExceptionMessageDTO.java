package com.topanotti.compassouolprojeto.exception.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExceptionMessageDTO {
    private Integer status_code;
    private String message;

    public ExceptionMessageDTO(Integer status_code, String message) {
        this.status_code = status_code;
        this.message = message;
    }
}