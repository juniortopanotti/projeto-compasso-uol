--liquibase formatted sql
--changeset gilberto.topanotti:001
CREATE SEQUENCE public.products_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

CREATE TABLE public.products (
	id int8 NOT NULL,
	name varchar(50) NOT NULL,
	description varchar(200) NOT NULL,
	price numeric(20,5) NOT NULL,
	CONSTRAINT produtos_pkey PRIMARY KEY (id)
);
--rollback DROP SEQUENCE products_seq;
--rollback DROP TABLE products;