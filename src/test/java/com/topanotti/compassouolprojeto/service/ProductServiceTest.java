package com.topanotti.compassouolprojeto.service;

import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.topanotti.compassouolprojeto.model.Product;
import com.topanotti.compassouolprojeto.repository.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
public class ProductServiceTest {

    @Mock
    ProductRepository productRepository;

    @InjectMocks
    ProductService productService;

    @Test
    public void whenSaveItShouldReturnUser() {
        Product persisted = new Product(null, "Cadeira", "Madeira", new BigDecimal(100));

        when(productRepository.save(ArgumentMatchers.any())).thenReturn(persisted);

        Product created = productService.save(persisted);

        assertThat(created.getDescription()).isSameAs(persisted.getDescription());
    }

    @Test
    public void whenUpdateItShouldReturnUser() {
        Product persisted = new Product(1L, "Cadeira", "Madeira", new BigDecimal(100));

        when(productRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(persisted));
        when(productRepository.save(ArgumentMatchers.any())).thenReturn(persisted);

        Product created = productService.save(persisted);

        assertThat(created.getDescription()).isSameAs(persisted.getDescription());
    }

    @Test
    public void whenGetItShouldReturnUser() {
        Product persisted = new Product(1L, "Cadeira", "Madeira", new BigDecimal(100));

        when(productRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(persisted));

        Product product = productService.get(1L);

        assertThat(product.getDescription()).isSameAs(persisted.getDescription());
    }

    @Test
    public void whenGetAllItShouldReturnUser() {
        List<Product> productList = new ArrayList<Product>();
        Product product = new Product(1L, "Cadeira", "Ferro", new BigDecimal(100));
        Product product2 = new Product(2L, "Cadeira", "Ferro", new BigDecimal(100));
        productList.add(product);
        productList.add(product2);

        when(productRepository.findAll()).thenReturn(productList);

        List<Product> products = productService.getAll();

        assertThat(products.get(0).getDescription()).isSameAs(productList.get(0).getDescription());
    }

    @Test
    public void whenGetAllSearchItShouldReturnUser() {
        List<Product> productList = new ArrayList<Product>();
        Product product = new Product(1L, "Cadeira", "Ferro", new BigDecimal(100));
        Product product2 = new Product(2L, "Cadeira", "Ferro", new BigDecimal(100));
        productList.add(product);
        productList.add(product2);
        Iterable<Product> iterable = new ArrayList<>(productList);

        when(productRepository.findAll(ArgumentMatchers.any(Predicate.class))).thenReturn(iterable);

        List<Product> products = productService.getSearch(Optional.of(new BigDecimal(10)), Optional.of(new BigDecimal(10)), Optional.of("search"));

        assertThat(products.get(0).getDescription()).isSameAs(productList.get(0).getDescription());
    }



    @Test
    public void whenDeleteItShouldBeOk() {
        Product persisted = new Product(1L, "Cadeira", "Madeira", new BigDecimal(100));

        when(productRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(persisted));

        productService.delete(1L);

        verify(productRepository, times(1)).deleteById(1L);
    }
}
