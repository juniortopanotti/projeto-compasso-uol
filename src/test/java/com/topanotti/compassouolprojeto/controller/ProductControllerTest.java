package com.topanotti.compassouolprojeto.controller;

import com.google.gson.Gson;
import com.topanotti.compassouolprojeto.model.Product;
import com.topanotti.compassouolprojeto.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
public class ProductControllerTest {
    public static final String PRODUCT_BASE_URI = "/products";

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private ProductService productService;


    @Test
    public void saveProductShouldReturnSuccessStatusWhenWithoutErrors() throws Exception {
        Product product = new Product(null, "Cadeira", "Madeira", new BigDecimal(100));
        Product saved = new Product(1L, "Cadeira", "Madeira", new BigDecimal(100));
        String requestBody = new Gson().toJson(product);
        when(productService.save(any(Product.class))).thenReturn(saved);

        ResultActions resultActions = mockMvc.perform(aPost(PRODUCT_BASE_URI, requestBody));

        resultActions.andExpect(status().is(HttpStatus.CREATED.value())).andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void updateProductShouldReturnSuccessStatusWhenWithoutErrors() throws Exception {
        Product persisted = new Product(1L, "Cadeira", "Madeira", new BigDecimal(100));
        Product product = new Product(1L, "Cadeira", "Ferro", new BigDecimal(100));
        String requestBody = new Gson().toJson(product);
        when(productService.get(any(Long.class))).thenReturn(persisted);
        when(productService.save(any(Product.class))).thenReturn(product);

        ResultActions resultActions = mockMvc.perform(aPut(PRODUCT_BASE_URI + "/1", requestBody));

        resultActions.andExpect(status().is(HttpStatus.OK.value())).andExpect(jsonPath("$.description", is("Ferro")));
    }

    @Test
    public void deleteProductShouldReturnSuccessStatusWhenWithoutErrors() throws Exception {
        ResultActions resultActions = mockMvc.perform(aDelete(PRODUCT_BASE_URI + "/1"));
        resultActions.andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void getProductShouldReturnSuccessStatusWhenWithoutErrors() throws Exception {
        Product product = new Product(1L, "Cadeira", "Ferro", new BigDecimal(100));
        when(productService.get(any(Long.class))).thenReturn(product);
        ResultActions resultActions = mockMvc.perform(aGet(PRODUCT_BASE_URI + "/1"));
        resultActions.andExpect(status().is(HttpStatus.OK.value())).andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void getAllProductShouldReturnSuccessStatusWhenWithoutErrors() throws Exception {
        List<Product> productList = new ArrayList<Product>();
        Product product = new Product(1L, "Cadeira", "Ferro", new BigDecimal(100));
        Product product2 = new Product(2L, "Cadeira", "Ferro", new BigDecimal(100));
        productList.add(product);
        productList.add(product2);

        when(productService.getAll()).thenReturn(productList);
        ResultActions resultActions = mockMvc.perform(aGet(PRODUCT_BASE_URI));
        resultActions.andExpect(status().is(HttpStatus.OK.value())).andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)));
    }

    @Test
    public void getAllSearchProductShouldReturnSuccessStatusWhenWithoutErrors() throws Exception {
        List<Product> productList = new ArrayList<Product>();
        Product product = new Product(1L, "Cadeira", "Ferro", new BigDecimal(100));
        Product product2 = new Product(2L, "Cadeira", "Ferro", new BigDecimal(100));
        productList.add(product);
        productList.add(product2);

        when(productService.getSearch(any(Optional.class), any(Optional.class), any(Optional.class))).thenReturn(productList);
        ResultActions resultActions = mockMvc.perform(aGet(PRODUCT_BASE_URI+"/search"));
        resultActions.andExpect(status().is(HttpStatus.OK.value())).andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(2)));
    }

    private MockHttpServletRequestBuilder aPost(String route, String requestBody) {

        return post(route)
                .content(requestBody.getBytes())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

    }

    private MockHttpServletRequestBuilder aPut(String route, String requestBody) {

        return put(route)
                .content(requestBody.getBytes())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

    }

    private MockHttpServletRequestBuilder aDelete(String route) {

        return delete(route)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

    }

    private MockHttpServletRequestBuilder aGet(String route) {

        return get(route)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

    }


}
