Para rodar o projeto é necessário ter configurado no ambiente o banco de dados postgres com usuário e senha padrão postgres/postgres ou alterar o usuário e senha no application.yml

Também é possível rodar através do Docker.

Para executar o microserviço é necessário executar os seguintes comandos: 
`mvn install`
`mvn spring-boot:run`
